<?php

add_action( 'wp_enqueue_scripts', 'jb_theme_enqueue_styles' );

function jb_theme_enqueue_styles() {
    $parenthandle = 'twentytwentyonechild-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
    $theme = wp_get_theme();
    wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css',
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array( $parenthandle ),
        $theme->get('Version') // this only works if you have Version in the style header
    );
}

/**
 * Function includes
 */
require get_stylesheet_directory() . '/functions/override-gutenberg.php';