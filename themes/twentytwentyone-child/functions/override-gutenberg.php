<?php

/**
 * Gutenberg default block overrides
 *
 * Just list the blocks we would like to include
 *
 * Community list of all blocks
 * https://rudrastyh.com/gutenberg/remove-default-blocks.html#block_slugs
 *
 * Core list of all blocks
 * https://wordpress.org/support/article/blocks/
 *
 * Allowed blocks per post type
 * https://developer.wordpress.org/reference/hooks/allowed_block_types_all/#comment-5581
 *
 * Custom block framework
 * https://developer.wordpress.org/block-editor/reference-guides/packages/packages-create-block/
 */

function jb_allowed_block_types( $block_editor_context, $editor_context ) {

    if ( ! empty( $editor_context->post ) ) {
        switch( $editor_context->post->post_type ) {
            case 'post':
                return array(
                    'core/paragraph',
                    'core/heading',
                    'core/list',
                );
                break;
            case 'page':
                return array(
                    'create-block/image-header',
                    'create-block/image',
                    'create-block/text'
                );
                break;
            default:
                return true;
        }
    }

    return $block_editor_context;

}

add_filter( 'allowed_block_types_all', 'jb_allowed_block_types', 10, 2 );
